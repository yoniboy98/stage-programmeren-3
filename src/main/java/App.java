import Application.ProductApp;
import Utilities.CSVOutput;
import Utilities.CSVReader;

public class App {

    public static void main(String[] args) {
        CSVReader csvReader = new CSVReader();
        CSVOutput csvOutput = new CSVOutput();


        ProductApp productsApp = new ProductApp(csvReader, csvOutput);
        productsApp.startApp();
    }
}
