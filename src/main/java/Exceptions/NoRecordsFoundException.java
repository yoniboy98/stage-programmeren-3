package Exceptions;

public class NoRecordsFoundException extends Exception {
    String message = "er werden geen zoekresultaten gevonden";

    public NoRecordsFoundException() {
        super("Er ging iets fout, er werden geen resultaten gevonden");
    }

    public String getMessage() {
        return message;
    }
}
