package Exceptions;

public class NoFileException extends Exception {
    public NoFileException() {
        super("Gelieve eerst een file te importeren");
    }
}
