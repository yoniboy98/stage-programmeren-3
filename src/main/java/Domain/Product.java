package Domain;

import java.util.Objects;

public class Product {
    long articleId;
    String name;
    String category;
    String brand;
    double price;
    double number;

    public Product(long articleId, String name, String category, String brand, double price, double number) {
        this.articleId = articleId;
        this.name = name;
        this.category = category;
        this.brand = brand;
        this.price = price;
        this.number = number;
    }


    public long getArticleId() {
        return articleId;
    }

    public void setArticleId(long articleId) {
        this.articleId = articleId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getNumber() {
        return number;
    }

    public void setNumber(double number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return "Products{" +
                "articleId=" + articleId +
                ", name='" + name + '\'' +
                ", category='" + category + '\'' +
                ", brand='" + brand + '\'' +
                ", price=" + price +
                ", number=" + number +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return articleId == product.articleId &&
                Double.compare(product.price, price) == 0 &&
                Double.compare(product.number, number) == 0 &&
                name.equals(product.name) &&
                category.equals(product.category) &&
                brand.equals(product.brand);
    }

    @Override
    public int hashCode() {
        return Objects.hash(articleId, name, category, brand, price, number);
    }
}
