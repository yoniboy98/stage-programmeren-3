package Utilities;

import Console.ConsoleColors;
import Domain.Product;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class CSVReader {
    private ArrayList<Product> records;


    public CSVReader() {
        records = new ArrayList<>();
    }

    public Product getRecordNumber(int i) {
        return records.get(i);
    }

    public int getNumberOfRecords() {
        return records.size();
    }


    public ArrayList<Product> loadDataFromFile(String filepath) throws FileNotFoundException {

        BufferedReader br = new BufferedReader(new FileReader(filepath));
        String line = "";
        int counter = 0;
        Product product;
        try {
            while ((line = br.readLine()) != null) {
                if (!line.isEmpty()) {
                    String[] results = line.split(";");
                    if (counter > 0) {
                        long articleId = Long.parseLong(results[0]);
                        String name = results[3];
                        String category = results[6];
                        String brand = results[2];
                        double price = Double.parseDouble(results[11].replaceAll(",", "."));
                        double number = Double.parseDouble(results[12].replaceAll(",", "."));

                        product = new Product(articleId, name, category, brand, price, number);
                        records.add(product);
                    }
                }
                counter++;
            }
            System.out.println(ConsoleColors.CYAN_BOLD + "Importeren geslaagd" + ConsoleColors.RESET);

        } catch (Exception e) {
            System.err.println("Error Message: " + e.getMessage());
            System.err.println("Fout op lijnnummer: " + counter);

        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.getMessage();
                }
            }

        }
        return records;
    }

    public ArrayList<Product> getAllRecords() {
        return records;
    }

}

