package Utilities;


import Domain.Product;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;


public class Export {

    private static final String COMMA_DELIMITER = ",";
    private static final String LINE_SEPARATOR = "\n";
    private List<Product> productList;

    public Export(List<Product> productList) {
        this.productList = productList;
    }

    private static final String HEADER = "artikelnr,naam,categorie,merk,prijs,nummer";

    public void exportData(String filename, String path) throws IOException {
        ArrayList<String> csvLines = new ArrayList<>();
        StringBuilder oneCSVLine = new StringBuilder();

        //Adding the header
        oneCSVLine.append(HEADER);
        //New Line after the header
        csvLines.add(oneCSVLine.toString());

        //Iterate the empList
        for (Product product : productList) {
            oneCSVLine = new StringBuilder();
            oneCSVLine.append(String.valueOf(product.getArticleId()));
            oneCSVLine.append(COMMA_DELIMITER);
            oneCSVLine.append(product.getName());
            oneCSVLine.append(COMMA_DELIMITER);
            oneCSVLine.append(product.getCategory());
            oneCSVLine.append(COMMA_DELIMITER);
            oneCSVLine.append(product.getBrand());
            oneCSVLine.append(COMMA_DELIMITER);
            oneCSVLine.append(String.valueOf(product.getPrice()));
            oneCSVLine.append(COMMA_DELIMITER);
            oneCSVLine.append(String.valueOf(product.getNumber()));
            csvLines.add(oneCSVLine.toString());

        }
        Files.write(Paths.get(path + filename + ".csv"), csvLines, StandardCharsets.UTF_8, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
        System.out.println("Schrijven naar CSV-bestand geslaagd!!!");

    }
}



