package Application;


import Console.ConsoleColors;
import Domain.Product;
import Exceptions.NoFileException;
import Exceptions.NoRecordsFoundException;
import Services.PagesService;
import Services.SearchService;
import Services.SortService;
import Utilities.CSVOutput;
import Utilities.CSVReader;
import Utilities.Export;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;


public class ProductApp {
    private Scanner scanner;
    private Map<Integer, String> menuMap;
    private Map<Integer, String> searchMenuMap;
    private CSVReader csvReader;
    private CSVOutput csvOutput;
    private ArrayList<Product> allproducts = new ArrayList<>();
    private boolean continueApp = true;


    public ProductApp(CSVReader csvReader, CSVOutput csvOutput) {

        this.scanner = new Scanner(System.in);
        this.menuMap = new HashMap<>();
        this.csvReader = csvReader;
        this.csvOutput = csvOutput;
        this.searchMenuMap = new HashMap<>();

        menuMap.put(0, "sluit aplicatie");
        menuMap.put(1, "Importeer CSV file");
        menuMap.put(2, "Toon alle records");
        menuMap.put(3, "Toon alle records per pagina");
        menuMap.put(4, "Exporteer CSV file");
        menuMap.put(5, "Zoeken in records");
        menuMap.put(6, "Sorteren op prijs (Laag -> Hoog)");
        menuMap.put(7, "Sorteren op aantal (Laag -> Hoog)");

        searchMenuMap.put(0, ConsoleColors.BLUE + "Sluit het zoekmenu" + ConsoleColors.RESET);
        searchMenuMap.put(1, ConsoleColors.BLUE + "Zoek op naam" + ConsoleColors.RESET);
        searchMenuMap.put(2, ConsoleColors.BLUE + "Zoek op artikelnummer" + ConsoleColors.RESET);
        searchMenuMap.put(3, ConsoleColors.BLUE + "Zoek op merk" + ConsoleColors.RESET);
        searchMenuMap.put(4, ConsoleColors.BLUE + "Zoek op categorie" + ConsoleColors.RESET);
    }

    public void startApp() {
        System.out.println("******WELKOM******");
        System.out.println("-----Opties-----");
        showMenu();
    }

    private void showMenu() {
        for (Map.Entry<Integer, String> m : menuMap.entrySet()) {
            System.out.println(m.getKey() + " " + m.getValue());
        }
        chooseOption();
    }

    private void chooseOption() {
        if (scanner == null) {
            scanner = new Scanner(System.in);
        }
        try {
            System.out.println("Kies je optie: ");
            int i = scanner.nextInt();

            scanner.nextLine();
            executeOption(i);
        } catch (InputMismatchException | IOException e) {
            System.err.println("Niet geldig, gelieve een getal in te voeren");
            scanner = null;
            chooseOption();
        }
    }

    private void executeOption(int option) throws IOException {
        switch (option) {
            case 1:
                importData();
                break;
            case 2:
                showAllRecords();
                break;
            case 3:
                printRecordsPage(allproducts);
                break;

            case 4:
                exportData();
                break;

            case 5:
                showsearchMenu();
                break;

            case 6:
                sortOnPrice();
                break;
            case 7:
                sortOnAmount();
                break;
            case 0:
                closeGame();
                continueApp = false;
                break;

        }

        if (continueApp) {
            showMenu();
        }
    }

    private void searchOnName() throws NoRecordsFoundException, NoFileException {
        System.out.println(ConsoleColors.BLUE + "Op welke naam wilt u zoeken?" + ConsoleColors.RESET);
        ArrayList<Product> results = SearchService.searchOnName(askUserSearch(), allproducts);
        SortService.sortAlphabet(results);
        if (results.isEmpty() || results.size() == 0) {
            System.out.println("Er zijn geen overeenkomsten voor uw zoekopdracht");
            return;
        } else
            printRecordsPage(results);
    }

    private void searchOnArticleID() throws NoRecordsFoundException {
        System.out.println(ConsoleColors.BLUE + "Op welk artikelId wilt u zoeken?" + ConsoleColors.RESET);
        ArrayList<Product> results = SearchService.searchOnArticleId(askUserSearch(), allproducts);
        SortService.sortByArticleId(results);
        if (results.isEmpty() || results.size() == 0) {
            System.out.println("Er zijn geen overeenkomsten voor uw zoekopdracht");
            return;
        } else
            printRecordsPage(results);
    }


    private void searchOnBrand() throws NoRecordsFoundException {
        System.out.println(ConsoleColors.BLUE + "Op welk merk wilt u zoeken?" + ConsoleColors.RESET);
        ArrayList<Product> results = SearchService.searchOnBrand(askUserSearch(), allproducts);
        SortService.sortAlphabet(results);
        if (results.isEmpty() || results.size() == 0) {
            System.out.println("Er zijn geen overeenkomsten voor uw zoekopdracht");
            return;
        } else
            printRecordsPage(results);
    }

    private void searchOnCategory() throws NoRecordsFoundException {
        System.out.println(ConsoleColors.BLUE + "Op welke categorie wilt u zoeken?" + ConsoleColors.RESET);
        ArrayList<Product> results = SearchService.searchOnCategory(askUserSearch(), allproducts);
        SortService.sortAlphabet(results);
        if (results.isEmpty() || results.size() == 0) {
            System.out.println("Er zijn geen overeenkomsten voor uw zoekopdracht");
            return;
        } else
            printRecordsPage(results);
    }


    private String askUserSearch() {
        String search = scanner.nextLine();
        return search;
    }

    private String askUserLocation() {
        System.out.println(ConsoleColors.CYAN_BOLD + "Gelieve de locatie van de file in te vullen a.u.b" + ConsoleColors.RESET);
        String location = scanner.nextLine();
        return location;
    }

    private void importData() {
        try {
            allproducts = csvReader.loadDataFromFile(askUserLocation());

        } catch (FileNotFoundException e) {
            System.err.println("Bestandspad niet gevonden of niet juist ingevoerd" + "\n" + "Gelieve opnieuw te proberen");
            importData();
        }
    }


    private void exportData() throws IOException {
        System.out.println(ConsoleColors.CYAN_BOLD + "Geef een naam in voor het bestand" + ConsoleColors.RESET);
        Export export = new Export(csvReader.getAllRecords());

        String filename = scanner.nextLine();
        System.out.println(ConsoleColors.CYAN_BOLD + "Waar wil je dit bestand wegschrijven?" + ConsoleColors.RESET);
        String path = scanner.nextLine();
        export.exportData(filename, path);

    }

    private void closeGame() {

        System.out.println(ConsoleColors.YELLOW_BOLD + "De applicatie wordt gesloten" + ConsoleColors.RESET);
    }

    private void showAllRecords() {
        if (csvReader.getNumberOfRecords() == 0) {
            System.err.println("Er kunnen geen records getoond worden, gelieve eerst een file te importeren");
        } else
            for (int i = 0; i < csvReader.getNumberOfRecords(); i++) {
                ArrayList<Product> allProducts = csvReader.getAllRecords();
                csvOutput.printAllRecords(allProducts);
                break;
            }
    }

    private void sortOnPrice() {
        try {
            SortService.sortByPrice(allproducts);
            printRecordsPage(allproducts);
        } catch (Exception e) {
            System.err.println("Error: Gelieve eerst een file importeren");
        }
    }

    private void sortOnAmount() {
        SortService.sortByNumber(allproducts);
        printRecordsPage(allproducts);
    }

    private void printRecordsPage(ArrayList<Product> products) {
        if (allproducts.size() == 0 || allproducts.isEmpty()) {
            System.err.println("Error: Gelieve eerst een file importeren (Optie 1)");
        } else {
            ArrayList<ArrayList<Product>> listOfList = PagesService.createPages(products);
            recordsPerPage(listOfList, products.size(), listOfList.size());
        }
    }


    private void recordsPerPage(ArrayList<ArrayList<Product>> listOflist, Integer listSize, Integer pagesAmount) {
        for (int i = 0; i < listOflist.size(); i++) {
            int counterPage = i;
            ArrayList<Product> products = listOflist.get(i);
            csvOutput.printPage(products);
            System.out.println(ConsoleColors.CYAN_BOLD + "Pagina: " + (counterPage + 1) + "/" + pagesAmount + "\nTotaal aantal records: " + listSize
                    + ConsoleColors.RESET + "\n");
            counterPage++;
            if (pagesAmount > 1 && products.size() == 20) {
                System.out.println("Wilt u naar de volgende pagina? (ja/nee)");
                String answer = scanner.nextLine();
                if (answer.matches("ja|j|J|JA|Ja")) {
                    continue;
                } else if
                (answer.matches("nee|n|N|NEE|Nee|NEEN|neen|Neen"))
                    System.out.println(ConsoleColors.CYAN_BOLD + "U bent teruggekeerd naar het menu" +
                            ConsoleColors.RESET);

                break;
            }
        }
    }


    private void showsearchMenu() {
        if (allproducts.isEmpty() || allproducts.size() == 0) {
            System.err.println("Error: Gelieve eerst een file importeren (Optie 1)");
        } else {
            for (Map.Entry<Integer, String> m : searchMenuMap.entrySet()) {
                System.out.println(m.getKey() + " " + m.getValue());
            }
            chooseSearchOption();
        }
    }


    private void executeSearchOption(int option) throws NoRecordsFoundException, NoFileException {
        switch (option) {
            case 1:
                searchOnName();
                break;
            case 2:
                searchOnArticleID();
                break;
            case 3:
                searchOnBrand();
                break;
            case 4:
                searchOnCategory();
                break;
            case 0:
                closeSearchMenu();
                showMenu();
                return;
        }
        showsearchMenu();
    }


    private void chooseSearchOption() {
        if (scanner == null) {
            scanner = new Scanner(System.in);
        }
        try {
            System.out.println(ConsoleColors.BLUE + "Kies je optie: " + ConsoleColors.RESET);
            int i = scanner.nextInt();

            scanner.nextLine();
            executeSearchOption(i);
        } catch (InputMismatchException | NoRecordsFoundException | NoFileException e) {
            System.err.println("Niet geldig, gelieve een getal in te voeren");
            scanner = null;
            chooseSearchOption();
        }
    }

    private void closeSearchMenu() {
        System.out.println("Het zoekmenu wordt gesloten, u komt terug in het hoofdmenu");
    }
}