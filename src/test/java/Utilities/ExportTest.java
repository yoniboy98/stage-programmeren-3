package Utilities;

import Domain.Product;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class ExportTest {

    @Test
    public void exportDataTest() throws IOException {
        String defaultDirectory = ".\\Yoni\\";
        ArrayList<Product> products = new ArrayList<>();
        Product item = new Product(123456, "koelen", "huishoudproducten", "lipson", 120.00, 15);
        for (long i = 0; i < 25; i++) {
            products.add((int) i, item);
        }

        Export exportService = new Export(products);

        exportService.exportData("actual", defaultDirectory);
        Path file1 = new File(defaultDirectory + "actual.csv").toPath();
        Path file2 = new File(defaultDirectory + "expected.csv").toPath();
        byte[] actual = Files.readAllBytes(file1);
        byte[] expected = Files.readAllBytes(file2);
        assertArrayEquals(expected, actual);
    }

}